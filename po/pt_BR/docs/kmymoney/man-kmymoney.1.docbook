<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE refentry PUBLIC "-//KDE//DTD DocBook XML V4.5-Based Variant V1.1//EN" "dtd/kdedbx45.dtd" [
<!ENTITY kmymoney "<application
>KMyMoney</application
>">
<!ENTITY kappname "&kmymoney;">
<!ENTITY % Brazilian-Portuguese "INCLUDE">
]>

<refentry lang="&language;">
<refentryinfo>

<author
><firstname
>Michael</firstname
><othername
>T.</othername
><surname
>Edwardes</surname
> <contrib
>Conceito original, muito do código inicial. Administração do projeto</contrib
> <email
>mte@users.sourceforge.net</email
> </author>

<author
><firstname
>Thomas</firstname
><surname
>Baumgart</surname
> <contrib
>Gerenciador de versão e Mantenedor principal do código. Administrador do projeto</contrib
> <email
>ipwizard@users.sourceforge.net</email
> </author>

<author
><firstname
>Tony</firstname
><surname
>Bloomfield</surname
> <contrib
>Importador do GnuCash. Suporte a banco de dados.</contrib
> <email
>tonybloom@users.sourceforge.net</email
> </author>

<author
><firstname
>Robert</firstname
><surname
>Wadley</surname
> <contrib
>Artista. Ícones, telas de abertura, página inicial. Capturas de tela.</contrib
> <email
>robntina@users.sourceforge.net</email
> </author>

<author
><firstname
>Alvaro</firstname
><surname
>Soliverez</surname
> <contrib
>Previsão. Relatórios.</contrib
> <email
>asoliverez@users.sourceforge.net</email
> </author>

<author
><firstname
>Fernando</firstname
><surname
>Vilas</surname
> <contrib
>Suporte ao banco de dados.</contrib
> <email
>fvilas@users.sourceforge.net</email
> </author>

<author
><firstname
>Cristian</firstname
><surname
>Oneț</surname
> <contrib
>Correções e plugins.</contrib
> <email
>onet.cristian@gmail.com</email
> </author>

<author
><firstname
>Jack</firstname
><othername
>H.</othername
><surname
>Ostroff</surname
> <contrib
>Documentação.</contrib
> <email
>ostroffjh@users.sourceforge.net</email
> </author>

<author
><firstname
>Felix</firstname
><surname
>Rodriguez</surname
> <contrib
>Administração do projeto, versões anteriores.</contrib
> <email
>frodriguez@users.sourceforge.net</email
> </author>

<author
><firstname
>Kevin</firstname
><surname
>Tambascio</surname
> <contrib
>Suporte de investimento inicial.</contrib
> <email
>ktambascio@users.sourceforge.net</email
> </author>

<!-- This person was previously listed as an author here, but is not currently
     included in credits.docbook.  He should either be dropped from here or
     included there.

    <author
><firstname
>Arni</firstname
><surname
>Ingimundarson</surname>
    <contrib
>Developer, previous versions.</contrib>
    <email
>arniing@users.sourceforge.net</email
></author>

    These persons were not previously listed as authors here, but are included
    in credits.docbook.  They should either be dropped from there or included
    here.

    <author
><firstname
>Ace</firstname
><surname
>Jones</surname>
    <contrib
>Reports.  OFX Import.  Online Quotes.  Documentatation.  Previous versions.</contrib>
    <email
>acejones@users.sourceforge.net</email
></author>

    <author
><firstname
>John</firstname
><surname
>C</surname>
    <contrib
>Developer, previous versions.</contrib>
    <email
>tacoturtle@users.sourceforge.net</email
></author>

    There are eight additional people in credits.docbook for "Special Thanks."
    Should any of them be included here?  -->
</refentryinfo>

<refmeta>
<refentrytitle
><command
>kmymoney</command
></refentrytitle>
<manvolnum
>1</manvolnum>
<refmiscinfo class="source"
>&kmymoney;</refmiscinfo>
<refmiscinfo class="manual"
>Programas executáveis</refmiscinfo>
</refmeta>

<refnamediv>
<refname
><command
>kmymoney</command
></refname>
<refpurpose
>o gerenciador de finanças pessoais para o &kde;</refpurpose>
</refnamediv>

<refsynopsisdiv>
<cmdsynopsis
><command
>kmymoney</command
> <group
><option
>Opções Genéricas</option
></group
> <group
><option
>Opções do Qt</option
></group
> <group
><option
>Opções do KDE</option
></group
> <group
><option
>Opções</option
></group
> <group
><option
>Arquivo</option
></group
> </cmdsynopsis>
</refsynopsisdiv>

<refsect1>
<title
>Descrição</title>

<para
>O &kmymoney; é o Gerenciador de Finanças Pessoais do ambiente de trabalho &kde;. Ele fornece as funções necessárias para consolidar os seus cheques, gerenciar os seus empréstimos pessoais e classificar as suas receitas e despesas, usando uma interface gráfica semelhante a um livro de registros.</para>

</refsect1>

<refsect1>
<title
>Opções</title>

<variablelist>

<varlistentry>
<term
><replaceable
>Arquivo</replaceable
></term>
<listitem>
<para
>arquivo a abrir </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>-n</option
></term>
<listitem>
<para
>Iniciar sem abrir qualquer arquivo </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--timers</option
></term>
<listitem>
<para
>Ativar cronômetros de desempenho </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--nocatch</option
></term>
<listitem>
<para
>Não capturar globalmente exceções não capturadas </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--trace</option
></term>
<listitem>
<para
>Ativa o registro de chamadas do programa. Esta opção só está disponível quando o &kmymoney; está compilado no modo de depuração. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--dump-actions</option
></term>
<listitem>
<para
>Apresenta os nomes de todos os objetos KAction definidos para o 'stdout' e sai. Esta opção só está disponível quando o &kmymoney; está compilado no modo de depuração. </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--help</option
></term>
<listitem>
<para
>Mostra a ajuda sobre as opções </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--help-qt</option
></term>
<listitem>
<para
>Mostra as opções específicas do Qt </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--help-kde</option
></term>
<listitem>
<para
>Mostra as opções específicas do KDE </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--help-all</option
></term>
<listitem>
<para
>Mostra todas as opções </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--author</option
></term>
<listitem>
<para
>Mostra informações sobre o autor </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>-v, --version</option
></term>
<listitem>
<para
>Mostra informações sobre a versão </para>
</listitem>
</varlistentry>

<varlistentry>
<term
><option
>--license</option
></term>
<listitem>
<para
>Mostra informações sobre a licença </para>
</listitem>
</varlistentry>

</variablelist>

</refsect1>

<refsect1>
<title
>Ambiente</title>
<variablelist>

<varlistentry>
<term
>$<envar
>KDE_LANG</envar
></term>
<listitem>
<para
>A configuração regional do idioma a usar. Esta opção possibilita a definição de outro ambiente de idioma para um programa que não seja o padrão. O pacote correto para o idioma deverá estar instalado, para que esta opção funcione. O idioma usado por padrão, quando mais nenhuma estiver configurada, é o 'en_US'. Ao decidir qual o idioma, são pesquisados os seguintes recursos, nesta ordem: <envar
>KDE_LANG</envar
>, arquivo de configuração, <envar
>LC_CTYPE</envar
>, <envar
>LC_MESSAGES</envar
>, <envar
>LC_ALL</envar
>, <envar
>LANG</envar
>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>KDE_UTF8_FILENAMES</envar
></term>
<listitem>
<para
>Assume que todos os nomes dos arquivos estão no formato UTF-8, independentemente da configuração do idioma atual. Caso contrário, o formato dos nomes de arquivos é definido pelo idioma.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>KDE_MULTIHEAD</envar
></term>
<listitem>
<para
>Se esta variável tiver um valor lógico verdadeiro, é ativado o modo de telas múltiplas. A tela do KDE será compartilhada por mais de uma tela física.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>DISPLAY</envar
></term>
<listitem>
<para
>Indica a tela do X onde irá executar o KDE.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>SESSION_MANAGER</envar
></term>
<listitem>
<para
>O gerenciador de sessões a usar. Esta opção é definida automaticamente pelo KDE, sendo uma localização de rede para o 'socket' do gerenciador de sessões.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>HOME</envar
></term>
<listitem>
<para
>A localização da pasta pessoal do usuário atual.</para>
</listitem>
</varlistentry>

<varlistentry>
<term
>$<envar
>KDEHOME</envar
></term>
<listitem>
<para
>A pasta de configurações, por cada usuário, do KDE.</para>
</listitem>
</varlistentry>

</variablelist>
</refsect1>

<refsect1>
<title
>Veja também</title>

<para
>A documentação completa do &kmymoney; é mantida como um manual em DocBook. Se o programa &khelpcenter; estiver instalado de forma adequada na sua máquina, o comando <userinput
><command
>khelpcenter</command
> <parameter
>help:/kmymoney</parameter
></userinput
> deverá dar acesso ao manual completo. Em alternativa, o manual poderá ser consultado no &konqueror;, se indicar o <acronym
>URL</acronym
> <ulink url="help:/kmymoney"
>help:/kmymoney</ulink
></para>

</refsect1>

</refentry>
